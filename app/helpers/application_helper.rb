module ApplicationHelper
  def status_message(board)
    processed_data = board.process_board

    if processed_data[:found] == true
      winner_name(processed_data[:value], board)
    elsif board.draw?
      "It is a draw!"
    else
      whose_turn(board)
    end

  end

  def whose_turn(board)
    empty_cells = board.number_empty_cells
    if empty_cells.odd?
      "#{board.player_1}, it is your turn! :-)"
    else
      "#{board.player_2}, it is your turn! :-)"
    end
  end

  def winner_name(value, board)
    if value == "X"
      "The winner is #{board.player_1}!!!!"
    else
      "The winner is #{board.player_2}!!!!"
    end
  end
end
