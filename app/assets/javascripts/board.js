$(function () {

    $(".cell_class").hover(function() {
        if ($(this).data('empty') == 'y') {
            $(this).css('background-color', 'lightgreen');
            $(this).css('cursor', 'pointer');
        }
    }, function() {
        $(this).css('background-color', 'white');
        $(this).css('cursor','auto');
    });

    $('.cell_class').bind('click', function () {
        if ($(this).data('empty') == 'y') {
            $.ajax({
                type: "POST",
                url: $(this).attr('data-url'),
                data: { _method:'PUT' },
                dataType: "script"
            });
        }
    });
});
