class CellsController < ApplicationController

  before_action :set_board_and_cell

  def update
    process_and_set_value
    respond_to do |format|
      format.js { render layout: false }
    end
  end


  private

  def set_board_and_cell
    @board = Board.find(params[:board_id])
    @cell = @board.cells.find(params[:id])
  end

  def process_and_set_value
    processed_data = @board.process_board
    unless ((processed_data[:found] == true) || (@board.draw?) )
      empty_cells = @board.number_empty_cells
      if empty_cells.odd?
        @cell.value = "X"
      else
        @cell.value = "O"
      end
      @cell.save
      @board.reload
    end
  end
end
