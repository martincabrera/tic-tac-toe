# == Schema Information
#
# Table name: boards
#
#  id         :integer          not null, primary key
#  player_1   :string
#  player_2   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Board < ActiveRecord::Base

  #relations
  has_many :cells, dependent: :destroy

  #callbacks
  before_create :build_cell_grid

  #validations
  validates :player_1, :player_2, presence: true
  validate :players_with_different_names


  EMPTY_VALUE = ""


  def process_board
    result = check_rows
    result = check_columns unless result[:found]
    result = check_diagonals unless result[:found]
    result
  end

  def have_winner?
    result = process_board
    result[:found]
  end

  def draw?
    flatten_values = cells.map { |cell| cell.value }.uniq
    process_data = process_board
    if !flatten_values.include?(EMPTY_VALUE) && process_data[:found] == false
      true
    else
      false
    end
  end

  def number_empty_cells
    counter = 0
    cells.each do |cell|
      counter += 1 if cell.value == EMPTY_VALUE
    end
    counter
  end

  private


  def players_with_different_names
    if !self.player_1.nil? && !self.player_2.nil? && (self.player_1.strip == self.player_2.strip)
      errors.add(:player_1, 'must be different to player 2')
      errors.add(:player_2, 'must be different to player 1')
    end
  end

  def row_cells(row_number)
    cells.select { |cell| cell.row == row_number }
  end

  def column_cells(column_number)
    cells.select { |cell| cell.column == column_number }
  end

  def diagonal_cells(default_order = :direct)
    if default_order == :direct
      cells.select { |cell| cell.row == cell.column }
    else
      cells.select { |cell| cell.row == (4 - cell.column) }
    end
  end

  def check_rows
    result = {found: false, value: EMPTY_VALUE}
    (1..3).each do |row|
      uniq_values = row_cells(row).map { |cell| cell.value }.uniq
      result = check_uniq_value(uniq_values)
      return result if result[:found] == true
    end
    result
  end

  def check_columns
    result = {found: false, value: EMPTY_VALUE}
    (1..3).each do |row|
      uniq_values = column_cells(row).map { |cell| cell.value }.uniq
      result = check_uniq_value(uniq_values)
      return result if result[:found] == true
    end
    result
  end

  def check_diagonals
    result = {found: false, value: EMPTY_VALUE}
    uniq_values = diagonal_cells.map { |cell| cell.value }.uniq
    if one_value_not_empty?(uniq_values)
      result = {found: true, value: uniq_values[0]}
    else
      uniq_values = diagonal_cells(:revert).map { |cell| cell.value }.uniq
      if one_value_not_empty?(uniq_values)
        result = {found: true, value: uniq_values[0]}
      end
    end
    result
  end

  def build_cell_grid
    (1..3).each do |row|
      (1..3).each do |column|
        cells.build(row: row, column: column, value: EMPTY_VALUE)
      end
    end
    true
  end

  def check_uniq_value(uniq_values)
    result = {found: false, value: EMPTY_VALUE}
    if one_value_not_empty?(uniq_values)
      result = {found: true, value: uniq_values[0]}
    end
    result
  end

  def one_value_not_empty?(uniq_values)
    if uniq_values.size == 1 && uniq_values[0] != EMPTY_VALUE
      return true
    else
      return false
    end
  end
end
