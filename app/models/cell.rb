# == Schema Information
#
# Table name: cells
#
#  id         :integer          not null, primary key
#  board_id   :integer
#  row        :integer
#  column     :integer
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Cell < ActiveRecord::Base


  #relations
  belongs_to :board, touch: true

  #validations
  validates_inclusion_of :row, in: [1, 2, 3]
  validates_inclusion_of :column, in: [1, 2, 3]
  validates_inclusion_of :value, in: [ "X", "O", "" ]
end
