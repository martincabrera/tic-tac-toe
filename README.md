# README #


### TIC-TAC-TOE ###

This is a fully functional version of TicTacToe

* Ruby 2.1.1 & Rails 4.2.4

### Database setup (SQLite) ###

* rake db:create
* rake db:migrate

### Tests done with minitest ###

* rake test # to run all 13 tests and passing! :)