class CreateBoards < ActiveRecord::Migration
  def change
    create_table :boards do |t|
      t.string :player_1
      t.string :player_2
      t.string :winner

      t.timestamps null: false
    end
  end
end
