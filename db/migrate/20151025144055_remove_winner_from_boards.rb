class RemoveWinnerFromBoards < ActiveRecord::Migration
  def up
    remove_column :boards, :winner
  end

  def down
    add_column :boards, :winner, :string
  end
end
