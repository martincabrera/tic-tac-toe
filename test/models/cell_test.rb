# == Schema Information
#
# Table name: cells
#
#  id         :integer          not null, primary key
#  board_id   :integer
#  row        :integer
#  column     :integer
#  value      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require "test_helper"

class CellTest < ActiveSupport::TestCase

  def setup
    board = Board.create(player_1: "Player 1", player_2: "Player 2")
    @cell = board.cells.first
  end

  test 'new cell has empty string value' do
    assert_equal "", @cell.value
  end

end



