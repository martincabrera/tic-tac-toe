# == Schema Information
#
# Table name: boards
#
#  id         :integer          not null, primary key
#  player_1   :string
#  player_2   :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'test_helper'

class BoardTest < ActiveSupport::TestCase

  def setup
    @board = Board.create(player_1: "Player 1", player_2: "Player 2")
  end

  test 'new board has 9 cells' do
    assert_equal 9, @board.cells.size
  end

  test 'new board does not have a winner' do
    assert_equal false, @board.have_winner?
  end

  test 'board with first row all to X has a winner' do
    @board.cells.where(row: 1).each do |cell|
      cell.value = "X"
      cell.save
    end
    result = @board.process_board
    assert_equal true, result[:found]
    assert_equal "X", result[:value]
  end

  test 'board with first column all to O has a winner' do
    @board.cells.where(column: 1).each do |cell|
      cell.value = "O"
      cell.save
    end
    result = @board.process_board
    assert_equal true, result[:found]
    assert_equal "O", result[:value]
  end

  test 'board with diagonal full of X has a winner' do
    (1..3).each do |number|
      cell = @board.cells.where(row: number, column: number).first
      cell.value = "X"
      cell.save
    end
    result = @board.process_board
    assert_equal true, result[:found]
    assert_equal "X", result[:value]
  end

  #Board Setting
  # X O O
  # O X X
  # O X O
  test 'board has a draw' do
    setting = {
        1 => { 1 => "X", 2 => "O", 3 => "0" },
        2 => { 1 => "O", 2 => "X", 3 => "X" },
        3 => { 1 => "O", 2 => "X", 3 => "O" }
    }
    @board.cells.each do |cell|
      cell.value = setting[cell.row][cell.column]
      cell.save
    end
    assert_equal true, @board.draw?
  end

  test 'board players cannot be empty' do
    board = Board.new
    assert_not board.save, "Saved the board without players names"
  end

end
