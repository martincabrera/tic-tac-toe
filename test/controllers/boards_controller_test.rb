require 'test_helper'

class BoardsControllerTest < ActionController::TestCase
  setup do
    @player_1_name = "name_1"
    @board = Board.create(player_1: @player_1_name, player_2: "name_2")
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:boards)
  end

  test "should get new" do
    get :new
    assert_response :success
    assert_template "new"
  end

  test "should render new template" do
    get :new
    assert_template :new
    assert_template layout: "layouts/application"
  end


  test "should show board" do
    get :show, id: @board.id
    assert_response :success

  end


  test "should destroy board" do
    assert_difference('Board.count', -1) do
      delete :destroy, id: @board
    end

    assert_redirected_to boards_path
  end
end
