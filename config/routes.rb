Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'boards#new'

  resources :boards do
    resources :cells, only: :update, defaults: { format: 'js' }
  end


end
